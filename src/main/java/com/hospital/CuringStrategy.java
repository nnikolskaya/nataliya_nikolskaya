package com.hospital;

public interface CuringStrategy {
    default int getOrder(){
        return 2;
    }
    void useMedicine(Patients patients);
}
