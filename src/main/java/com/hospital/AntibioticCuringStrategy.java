package com.hospital;

public class AntibioticCuringStrategy implements CuringStrategy {

    private static AntibioticCuringStrategy antibiotic;

    @Override
    public void useMedicine(final Patients patients) {
        patients.movePatientsToStatus(HealthStatus.T, HealthStatus.H);
    }

    public static AntibioticCuringStrategy get(){
        if(antibiotic == null){
            antibiotic = new AntibioticCuringStrategy();
        }
        return antibiotic;
    }
}
