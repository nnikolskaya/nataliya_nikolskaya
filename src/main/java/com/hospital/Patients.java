package com.hospital;

import java.util.EnumMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

public class Patients {

    private Map<HealthStatus, Integer> patients = new EnumMap<>(HealthStatus.class);

    Patients(String subjects){
        for (String s : subjects.split(",")) {
            patients.put(HealthStatus.valueOf(s), patients.getOrDefault(HealthStatus.valueOf(s), 0) + 1);
        }
    }

    public void makeAllDie() {
        movePatientsToStatus(HealthStatus.F, HealthStatus.X);
        movePatientsToStatus(HealthStatus.H, HealthStatus.X);
        movePatientsToStatus(HealthStatus.T, HealthStatus.X);
        movePatientsToStatus(HealthStatus.D, HealthStatus.X);
    }


    public void movePatientsToStatus(@NotNull final HealthStatus d, @NotNull final HealthStatus x) {
        int firstStatus = patients.put(d, 0);
        patients.put(x, patients.getOrDefault(x, 0) + firstStatus);
    }

    public String showPatients(){
        return Stream.of(HealthStatus.values())
            .map(e -> e.name() + ":" + patients.getOrDefault(e, 0))
            .collect(Collectors.joining(" "));
    }
}
