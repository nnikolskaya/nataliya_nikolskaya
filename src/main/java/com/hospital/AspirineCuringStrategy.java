package com.hospital;

public class AspirineCuringStrategy implements CuringStrategy {

    private static AspirineCuringStrategy aspirine;

    @Override
    public void useMedicine(final Patients patients) {
        patients.movePatientsToStatus(HealthStatus.F, HealthStatus.H);
    }

    public static AspirineCuringStrategy get(){
        if(aspirine == null){
            aspirine = new AspirineCuringStrategy();
        }
        return aspirine;
    }
}
