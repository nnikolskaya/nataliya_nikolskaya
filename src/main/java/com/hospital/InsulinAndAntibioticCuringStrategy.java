package com.hospital;

public class InsulinAndAntibioticCuringStrategy implements CuringStrategy {

    private static InsulinAndAntibioticCuringStrategy insulinAndAntibiotic;

    public static InsulinAndAntibioticCuringStrategy get(){
        if(insulinAndAntibiotic == null){
            insulinAndAntibiotic = new InsulinAndAntibioticCuringStrategy();
        }
        return insulinAndAntibiotic;
    }

    @Override
    public void useMedicine(final Patients patients) {
        patients.movePatientsToStatus(HealthStatus.H, HealthStatus.F);
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
