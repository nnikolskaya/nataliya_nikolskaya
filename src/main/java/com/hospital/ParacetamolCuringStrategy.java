package com.hospital;

public class ParacetamolCuringStrategy implements CuringStrategy {

    private static ParacetamolCuringStrategy paracetamolCuring;

    public static ParacetamolCuringStrategy get(){
        if(paracetamolCuring == null){
            paracetamolCuring = new ParacetamolCuringStrategy();
        }
        return paracetamolCuring;
    }

    @Override
    public void useMedicine(final Patients patients) {
        patients.movePatientsToStatus(HealthStatus.F, HealthStatus.H);
    }
}
