package com.hospital;

public class NoInsulinCuringStrategy implements CuringStrategy {

    private static NoInsulinCuringStrategy noInsulin;
    @Override
    public int getOrder() {
        return 0;
    }
    public static NoInsulinCuringStrategy get(){
        if(noInsulin == null){
            noInsulin = new NoInsulinCuringStrategy();
        }
        return noInsulin;
    }

    @Override
    public void useMedicine(final Patients patients) {
        patients.movePatientsToStatus(HealthStatus.D, HealthStatus.X);
    }
}
