package com.hospital;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Quarantine {

    /**
     * To perform this exercise, you will use a readable, maintainable & pragmatic coding style.
     * Please remember that how it's done is as important as the end result.
     */

    private Patients patients;
    private Set<CuringStrategy> usedMedicine;


    public Quarantine(String subjects) {
        patients = new Patients(subjects);
        clearMedicine();
    }

    public void aspirin() {
        usedMedicine.add(AspirineCuringStrategy.get());
        if(hasMedicine(ParacetamolCuringStrategy.class)){
            patients.makeAllDie();
        }
    }

    public void antibiotic() {
        usedMedicine.add(AntibioticCuringStrategy.get());
    }

    public void insulin() {
        usedMedicine = usedMedicine.stream()
            .filter(p -> !NoInsulinCuringStrategy.class.equals(p.getClass()))
            .collect(Collectors.toSet());
        if(hasMedicine(AntibioticCuringStrategy.class)){
            usedMedicine.add(InsulinAndAntibioticCuringStrategy.get());
        }
    }

    public void paracetamol() {
        usedMedicine.add(new ParacetamolCuringStrategy());
        if(!hasMedicine(NoInsulinCuringStrategy.class)){
            usedMedicine.add(InsulinAndAntibioticCuringStrategy.get());
        }
        if(hasMedicine(AspirineCuringStrategy.class)){
            patients.makeAllDie();
        }
    }

    public void wait40Days() {
        usedMedicine.stream()
            .sorted(Comparator.comparingInt(CuringStrategy::getOrder))
            .forEach(p->p.useMedicine(patients));
        clearMedicine();
    }

    public String report() {
        return patients.showPatients();
    }

    private boolean hasMedicine(final Class medicine) {
        return usedMedicine.stream().anyMatch(p -> medicine.equals(p.getClass()));
    }

    private void clearMedicine() {
        usedMedicine = new HashSet<>();
        usedMedicine.add(new NoInsulinCuringStrategy());
    }
}
